using CSV
using DataFrames
using TextAnalysis
using MAT
using SparseArrays

function preprocess()
    rawdatafile = "data/winemag-data-130k-v2.csv"

    # Read CSV file, put data in a DataFrame, and filter by country
    rawdata = CSV.read(rawdatafile, DataFrame)
    dropmissing!(rawdata, :country)
    dropmissing!(rawdata, :taster_name)
    francedata = rawdata[rawdata.country.=="France", :]
    descrs = francedata.description

    # Save the DataFrame of country-filtered reviews
    CSV.write("data/francedata.csv", francedata)

    # Build corpus and preprocess
    mycorpus = Corpus([StringDocument(mystr) for mystr in descrs])
    prepare!(mycorpus, strip_non_letters | strip_case | strip_articles | strip_stopwords)
    update_lexicon!(mycorpus)
    update_inverse_index!(mycorpus)

    # Compute document-term and tf-idf matrices
    mydtm = DocumentTermMatrix(mycorpus)
    mytfidf = tf_idf(mydtm)

    # Write as a .mat file (Matlab)
    file = matopen("data/WineReviewsFr.mat", "w")
    write(file, "Xdtm", SparseMatrixCSC(mydtm.dtm))
    write(file, "Xtfidf", mytfidf)
    write(file, "terms", mydtm.terms)
    close(file)
end

preprocess()
