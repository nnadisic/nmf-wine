# From Dorian
function extract_topics(W, wordsList; bestWords=10, minValue=0)
    # Return an array of array containing for each sub-array the best worlds describing each topic
    # Input:
    #	-> W the NMF decomposition such that X = WH
    #	-> wordsList and array of string containing the associated words of X
    #	-> bestWords: the maximum number of world to keep
    #	-> minValue: parameter allowing to select only the words with a weight greater than "minValue"
    topics = Array{Array}(undef, size(W,2))
    for c in 1:size(W,2)
        bestPos = [pos for (pos, value) in sort(collect(zip(1:size(W,1), W[:,c])), rev=true, by=last) if value >= minValue] #by=x->(x[2])
        topics[c] = [string(i) for i in wordsList[bestPos][1:min(bestWords, length(bestPos))]]
    end
    return topics
end


# Given a vector of things, return a dictionary containing the number of occurences of every thing
function countstuff(A)
    mydict = Dict{String, Integer}()
    for element in unique(A)
        mydict[element] = count(==(element), A)
    end
    return sort(mydict, byvalue=true, rev=true)
end


# Same, but proportionnaly to total occurences given by some dictionary
function countstuff_proportion(A, dict)
    mydictprop = Dict{String, Float64}()
    for element in unique(A)
        mydictprop[element] = round(100*count(==(element), A)/dict[element], digits=3)
    end
    return sort(mydictprop, byvalue=true, rev=true)
end
