# Copied and modified from GIANT.jl

function halsW!(M::AbstractMatrix{T},
                W::AbstractMatrix{T},
                H::AbstractMatrix{T},
                HMt::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
                HHt::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
                lambda::T=zero(T)) where T <: Real
    halsH!(M', H', W', HMt, HHt, lambda)
end


function halsH!(M::AbstractMatrix{T},
                W::AbstractMatrix{T},
                H::AbstractMatrix{T},
                WtM::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
                WtW::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
                lambda::T=zero(T)) where T <: Real
    # Init
    r = size(H, 1)
    # If needed, compute intermediary values
    if length(WtM) == 0
        WtM = W' * M
    end
    if length(WtW) == 0
        WtW = W' * W
    end

    # Loop on rows of H
    for i in 1:r
        irowH = view(H, i, :)'
        deltaH = max.((WtM[i,:]' - WtW[i,:]' * H .- lambda)
                      / WtW[i,i], -irowH)
        irowH .= irowH .+ deltaH
        # Safety procedure
        if iszero(irowH)
            irowH .= 1e-15
        end
    end
end


function nmf(X::AbstractMatrix{T},
             r::Integer;
             maxiter::Integer=100,
             W0::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
             H0::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
             updaterW!::Function=halsW!,
             updaterH!::Function=halsH!) where T <: Real

    # Constants
    m, n = size(X)

    # If not provided, init W and H randomly
    W0 = length(W0) == 0 ? rand(m, r) : W0
    W = copy(W0)
    H0 = length(H0) == 0 ? rand(r, n) : H0
    H = copy(H0)

    # Alternate optimization
    for _ in 1:maxiter
        updaterW!(X, W, H)
        updaterH!(X, W, H)
        # Normalize
        for col in eachcol(H)
            col ./= norm(col, 1)
        end
    end

    return W, H
end
