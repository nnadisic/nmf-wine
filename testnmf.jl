using MAT
using CSV
using DataFrames
using LinearAlgebra
using SparseArrays

include("util.jl")
include("nmf.jl")

# Variables
dffilename = "data/francedata.csv"
datafilename = "data/WineReviewsFr.mat"
datavar = "Xtfidf"
datavar_dtm = "Xdtm"
termsvar = "terms"
rank = 10

# Read data
file = matopen(datafilename)
Xtfidf = read(file, datavar)
Xtfidf = Xtfidf'
Xdtm = read(file, datavar_dtm)
Xdtm = SparseMatrixCSC(Xdtm')
terms = read(file, termsvar)
close(file)
# display(Xtfidf)
# display(terms)

# Choose if working with document-term matrix or tf-idf
X = Xtfidf

# Load raw data for analysis
rawdata = CSV.read(dffilename, DataFrame)

nbitem_per_province = countstuff(rawdata.province)
nbitem_per_variety = countstuff(rawdata.variety)
nbitem_per_taster = countstuff(rawdata.taster_name)

# Run NMF
@time W, H = nmf(X, rank)
topics = extract_topics(W, terms)

# Output
println("########## Sparsity")
println("$(sum(H .> 10e-12)) / $(length(H)) = $(sum(H .> 10e-12) / length(H))\n")

println("########## PROVINCES")
for i in 1:length(topics)
    display(topics[i])
    display(countstuff_proportion(rawdata[(H[i,:] .> 0.05), :province], nbitem_per_province))
    println()
end

# println("########## CEPAGES")
# for i in 1:length(topics)
#     display(topics[i])
#     display(countstuff_proportion(rawdata[(H[i,:] .> 0.05), :variety], nbitem_per_variety))
#     println()
# end

# println("########## TASTERS")
# for i in 1:length(topics)
#     display(topics[i])
#     display(countstuff_proportion(rawdata[(H[i,:] .> 0.05), :taster_name], nbitem_per_taster))
#     println()
# end
